class GameModel {
  // late int GameID;
  late String Image;
  late String Title;
  late String Detail;
  GameModel(
    this.Image,
    this.Detail,
    this.Title,
  );
  GameModel.map(dynamic obj) {
    // this.GameID = obj['GameID'];
    this.Image = obj['Image'];
    this.Title = obj['Title'];
    this.Detail = obj['Detail'];
  }
  // int get _GameID => GameID;
  String get _Image => Image;
  String get _Title => Title;
  String get _Detail => Detail;
  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map['Image'] = _Image;
    map['Title'] = _Title;
    map['Detail'] = _Detail;
    // if (GameID != null) {
    //   map['GameID'] = GameID;
    // }
    return map;
  }

  GameModel.fromMap(Map<String, dynamic> map) {
    // this.GameID = map['GameID'];
    this.Image = map['Image'];
    this.Title = map['Title'];
    this.Detail = map['Detail'];
  }
}
