class UserModel {
  String UserName;
  String PassWord;
  String? Email;

  UserModel(this.UserName, this.PassWord, {this.Email});
  // UserModelLogin(
  //   this.UserName,
  //   this.PassWord,
  // )

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      'UserName': UserName,
      'Email': Email,
      'PassWord': PassWord,
    };
    return map;
  }

  static fromMap(Map<String, dynamic> map) {
    String UserName = map['UserName'] ?? "";
    String Email = map['Email'];
    String PassWord = map['PassWord'] ?? "";
    return UserModel(UserName, PassWord, Email: Email);
  }
}
