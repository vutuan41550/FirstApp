import 'package:flutter/material.dart';
import 'package:flutter_application_1/pages/add_game.dart';
import 'package:flutter_application_1/pages/channel1.dart';
import 'package:flutter_application_1/pages/channel_page.dart';
import 'package:flutter_application_1/pages/landing_page.dart';
import 'package:flutter_application_1/pages/widget/channel_page2.dart';

import 'Model/GameModel.dart';
import 'dataBaseHandler/dbHelper.dart';

void main() async {
  // var db = Dbhelper();
  // int gameSaved = await db.SaveGame(new GameModel('zxc', 'zxc', 'zxc'));
  // print("saved: $gameSaved");

  // game = await db.GetAllGame();
  // for (int i = 0; i < game.length; i++) {
  //   GameModel gameModel = GameModel.map(game[i]);
  //   print(
  //       '${gameModel.GameID} - ${gameModel.Image} - ${gameModel.Title} - ${gameModel.Detail}');
  // }
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: LandingPage(),
    );
  }
}
