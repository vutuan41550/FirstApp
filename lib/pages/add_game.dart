import 'dart:ui';

import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_application_1/Model/GameModel.dart';
import 'package:flutter_application_1/Model/UserModel.dart';
import 'package:flutter_application_1/pages/login_page.dart';
import 'package:flutter_application_1/pages/widget/alert.dart';
import 'package:flutter_application_1/pages/widget/channel_page2.dart';
import 'package:flutter_application_1/pages/widget/getTextFormField.dart';
import '../dataBaseHandler/dbHelper.dart';
import '../values/app_images.dart';
import '../values/app_styles.dart';
import 'package:path_provider/path_provider.dart';
import 'package:toast/toast.dart';

class AddGame extends StatefulWidget {
  @override
  State<AddGame> createState() => _AddGameState();
}

class _AddGameState extends State<AddGame> {
  final formkey = new GlobalKey<FormState>();
  final _conImage = TextEditingController();
  final _conDetail = TextEditingController();
  final _conTitle = TextEditingController();

  bool _passwordVisible = false;
  bool _rpasswordVisible = false;

  @override
  void initState() {
    _passwordVisible = false;
    _rpasswordVisible = false;
    super.initState();
    Dbhelper();
  }

  signup() {
    // String userID = _conUserID.text;
    String image = _conImage.text;
    String detail = _conDetail.text;
    String title = _conTitle.text;
    if (formkey.currentState!.validate()) {
      formkey.currentState!.save();
      GameModel model = GameModel(image, detail, title);
      Dbhelper().SaveGame(model).then((userData) {
        alertDialog('Successful save');
        Navigator.push(
            context, MaterialPageRoute(builder: (_) => MyChannel2()));
      });
      ;
    }
  }

  @override
  Widget build(BuildContext context) {
    ToastContext().init(context);
    return Scaffold(
      // resizeToAvoidBottomInset: true,
      body: Form(
        key: formkey,
        child: new Stack(
          children: [
            Container(
              decoration: new BoxDecoration(
                image: new DecorationImage(
                    image: new ExactAssetImage(AppImages.image0),
                    fit: BoxFit.cover),
              ),
              child: new BackdropFilter(
                filter: new ImageFilter.blur(
                  sigmaX: 10,
                  sigmaY: 10,
                ),
                child: new Container(
                  decoration:
                      new BoxDecoration(color: Colors.white.withOpacity(0.0)),
                ),
              ),
            ),
            SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Column(
                children: [
                  Container(
                    padding: const EdgeInsets.only(top: 30),
                    child: Text(
                      'Hello',
                      style: AppStyles.h3.copyWith(
                          fontSize: 50,
                          color: Colors.white,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Container(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 1.0, bottom: 60),
                      child: Text(
                        'Sign up!',
                        style: AppStyles.h3.copyWith(
                            fontSize: 50,
                            color: Colors.white,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  new Stack(
                    children: [
                      Container(
                        alignment: Alignment.bottomCenter,
                        height: 650,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(60),
                                topRight: Radius.circular(60))),
                      ),
                      Column(
                        children: [
                          SizedBox(
                            height: 40,
                          ),
                          // GetTextFormField(
                          //   controller: _conUserID,
                          //   labelName: 'UserID',
                          //   hintName: 'UserID',
                          //   icon: Icons.person,
                          //   inputType: TextInputType.name,
                          //   isObscureText: false,
                          // ),
                          GetTextFormField(
                            controller: _conImage,
                            icon: Icons.person,
                            inputType: TextInputType.name,
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          GetTextFormField(
                            controller: _conDetail,
                            icon: Icons.email,
                            isObscureText: false,
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          GetTextFormField(
                            controller: _conTitle,
                            icon: Icons.lock,
                            isObscureText: false,
                          ),
                          SizedBox(
                            height: 15,
                          ),

                          // GetTextFormField(
                          //   controller: _conPW,
                          //   labelName: 'Password',
                          //   hintName: 'Password',
                          //   icon: Icons.lock,
                          //   icon1: ,
                          //   // inputType: TextInputType.visiblePassword,
                          //   isObscureText: true,
                          // ),

                          // GetTextFormField(
                          //   controller: _conRPW,
                          //   labelName: 'Confirm Password',
                          //   hintName: 'Confirm Password',
                          //   icon: Icons.lock,
                          //   // inputType: TextInputType.visiblePassword,
                          //   isObscureText: true,
                          // ),
                          SizedBox(
                            height: 50,
                          ),
                          Container(
                            width: 300,
                            child: ElevatedButton(
                              child: Padding(
                                padding: const EdgeInsets.all(15.0),
                                child: Text(
                                  'SIGN UP',
                                  style: AppStyles.h3.copyWith(
                                      fontSize: 25,
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                              style: ButtonStyle(
                                backgroundColor: MaterialStateProperty.all(
                                    Color.fromARGB(255, 16, 50, 105)),
                                shape: MaterialStateProperty.all(
                                    RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(40),
                                )),
                              ),
                              onPressed: () {
                                signup();
                              },
                            ),
                          ),
                          Container(
                            padding: const EdgeInsets.only(top: 50, right: 50),
                            alignment: Alignment.bottomRight,
                            child: Text(
                              "Have account?",
                              style: AppStyles.h3.copyWith(
                                fontSize: 18,
                                color: Color.fromARGB(255, 154, 150, 150),
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                          Container(
                            padding: const EdgeInsets.only(right: 45),
                            alignment: Alignment.centerRight,
                            child: TextButton(
                              child: Text(
                                'Sign in',
                                style: AppStyles.h3.copyWith(
                                    fontSize: 20,
                                    color: Color.fromARGB(255, 20, 76, 121),
                                    fontWeight: FontWeight.w800),
                              ),
                              style: ButtonStyle(),
                              onPressed: () {
                                Navigator.pushAndRemoveUntil(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => LoginPage()),
                                    (route) => false);
                              },
                            ),
                          ),
                        ],
                      ),
                    ],
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
