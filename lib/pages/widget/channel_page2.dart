library blur_bottom_bar;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_application_1/Model/GameModel.dart';
import 'package:flutter_application_1/dataBaseHandler/dbHelper.dart';
import 'package:flutter_application_1/values/app_colors.dart';
import 'package:flutter_application_1/values/app_icons.dart';
import 'package:flutter_application_1/values/app_images.dart';
import 'package:flutter_application_1/values/app_styles.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

import 'dart:ui';

import 'package:shared_preferences/shared_preferences.dart';

class MyChannel2 extends StatefulWidget {
  final bool showSelectedLabels;
  final bool showUnselectedLabels;
  final VoidCallback? onClick;
  final double filterX;
  final double filterY;
  final double opacity;
  final Color backgroundColor;
  final BottomNavigationBarType bottomNavigationBarType;
  final int currentIndex;
  // final List<BottomNavigationBarItem> bottomNavigationBarItems;
  final Color selectedItemColor;
  final Color unselectedItemColor;
  // final Function(int) onIndexChange;
  const MyChannel2(
      {Key? key,
      this.showSelectedLabels = false,
      this.showUnselectedLabels = false,
      this.onClick,
      this.filterX = 5.0,
      this.filterY = 10.0,
      this.opacity = 0.8,
      this.backgroundColor = Colors.black,
      this.bottomNavigationBarType = BottomNavigationBarType.fixed,
      this.currentIndex = 0,
      // required this.onIndexChange,
      // required this.bottomNavigationBarItems,
      this.selectedItemColor = Colors.blue,
      this.unselectedItemColor = Colors.white})
      : super(key: key);

  @override
  State<MyChannel2> createState() => _MyChannel2State();
}

class _MyChannel2State extends State<MyChannel2> {
  List<GameModel> list_Game = [];
  bool fetching = true;
  late final GameModel game1;

  @override
  void initState() {
    _query();
    getAllGame();
    super.initState();
  }

  void getAllGame() async {
    list_Game = await Dbhelper().getAllGame();
    setState(() {
      fetching = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
        resizeToAvoidBottomInset: false,
        extendBody: true,
        body: Column(
          children: [
            SizedBox(
              height: 100,
            ),
            SingleChildScrollView(
              child: Container(
                height: size.height * 1 / 3.5,
                child: fetching
                    ? Center(
                        child: CircularProgressIndicator(),
                      )
                    : ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemCount: list_Game.length,
                        itemBuilder: (context, index) =>
                            DataCard(data: list_Game[index]),
                      ),
              ),
            ),
          ],
        ));
  }
}

class DataCard extends StatelessWidget {
  const DataCard({Key? key, required this.data}) : super(key: key);
  final GameModel data;
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 300,
      child: Padding(
        padding: const EdgeInsets.only(left: 24),
        child: Container(
          decoration: BoxDecoration(
            // color: AppColors.primaryColor,
            borderRadius: BorderRadius.circular(45.0),
          ),
          child: Stack(
            children: [
              Container(
                child: ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(20.0)),
                  child: Image.network(
                    data.Image,
                    fit: BoxFit.fitHeight,
                    width: 400,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 120.0),
                child: Container(
                  height: 100,
                  width: 247,
                  child: Center(
                    child: ClipRect(
                      child: BackdropFilter(
                        filter: ImageFilter.blur(sigmaX: 3.0, sigmaY: 3.0),
                        child: Container(
                          decoration: BoxDecoration(
                              color: Colors.grey.shade200.withOpacity(0.5)),
                          child: Row(
                            children: [
                              Container(
                                child: Column(
                                  children: [
                                    Container(
                                      alignment: Alignment.centerLeft,
                                      padding: const EdgeInsets.only(
                                          left: 12.0, top: 12, bottom: 5),
                                      child: Text(data.Title,
                                          style: AppStyles.h3.copyWith(
                                              fontSize: 20,
                                              fontWeight: FontWeight.w700,
                                              color: Colors.white)),
                                    ),
                                    Container(
                                      child: Padding(
                                        padding:
                                            const EdgeInsets.only(right: 55.0),
                                        child: Text(
                                          data.Detail,
                                          style: AppStyles.h3.copyWith(
                                              fontSize: 15,
                                              color: Colors.white),
                                        ),
                                      ),
                                    ),
                                    Container(
                                      child: Padding(
                                        padding: const EdgeInsets.only(
                                            top: 8.0, right: 85),
                                        child: Icon(
                                          Icons.star,
                                          color: Colors.amber,
                                          size: 18,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                padding:
                                    const EdgeInsets.only(left: 40.0, top: 30),
                                child: Container(
                                  child: ElevatedButton(
                                    child: Text(
                                      'Get',
                                      style: AppStyles.h3.copyWith(
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.white),
                                    ),
                                    style: ButtonStyle(
                                      shape: MaterialStateProperty.all(
                                          RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(20))),
                                      backgroundColor:
                                          MaterialStateProperty.all(
                                              AppColors.purple),
                                    ),
                                    onPressed: () {},
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

void _query() async {
  final allRows = await Dbhelper().QueryAllGame();
  print('query all row');
  allRows.forEach((row) {
    print(row);
    return null;
  });
}
